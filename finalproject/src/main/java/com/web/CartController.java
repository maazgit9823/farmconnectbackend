package com.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.CartDao;
import com.model.Cart;
import com.model.Customer;
import com.model.Products;

@RestController
public class CartController {

	@Autowired
	CartDao cartdao;
	
	 @GetMapping("/showAllCartProducts")
    public List<Cart> getAllCustomer() {
    	List<Cart> cartlist = cartdao.showAllCartProducts();
    	return cartlist;	
    }
	
	 @GetMapping("/showCartProductById/{cartId}")
	    public Cart getCartProductById(@PathVariable("cartId") int cartId ) {
		 Cart cart = cartdao.showCartProductById(cartId);
	    	return cart;	
	    }
	 
	 @PostMapping("/addProductstoCart")
	    public Cart addProductstoCart(@RequestBody Cart cart) {
	    	return cartdao.addProductstoCart(cart);	
	    }
	 
}
