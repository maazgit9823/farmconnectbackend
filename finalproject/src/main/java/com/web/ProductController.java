package com.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.ProductDao;
import com.model.Products;

@RestController
public class ProductController {

	@Autowired
    ProductDao productDao;
	
	@GetMapping("/showAllProducts")
	public List<Products> showAllProducts() {
   List<Products> prodList = productDao.getAllProducts();
    return prodList;
	}
	
	@GetMapping("/showProductById/{product_Id}")
	public Products showProductById(@PathVariable("product_Id") int product_Id) {
	Products product = productDao.getProductById(product_Id);
		return product;
		
	}
	
	@GetMapping("/showProductByName/{product_Name}")
	public Products showProductByName(@PathVariable("product_Name")String product_Name) {
		return productDao.getProductByName(product_Name);
	}
    
	@PostMapping("/addProduct")
	public Products addProduct(@RequestBody Products products) {
	return productDao.addProduct(products);	
	}

	@DeleteMapping("/deleteProduct/{product_Id}")
	public void deleteProduct(@PathVariable("product_Id" )int product_Id) {
		
	 productDao.deleteProductById(product_Id);
		
	}
	
	@GetMapping("/showProductByfId/{farmer_Id}")
	public List<Products> showProductByfId(@PathVariable("farmer_Id") int farmer_Id) {
		List<Products> prodList = productDao.getProductByfId(farmer_Id);
		return prodList;
		
	}
	
}
