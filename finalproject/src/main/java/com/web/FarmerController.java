package com.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.EmailService;
import com.dao.FarmerDao;
import com.dao.SmsService;
import com.model.Customer;
import com.model.Farmer;

@RestController
public class FarmerController {
	private static final BCryptPasswordEncoder pwEncoder = new BCryptPasswordEncoder();
	
  @Autowired
  FarmerDao farmerDao;

  @Autowired
  SmsService smsService;
  
  @Autowired
  EmailService EmailService;
  
  @GetMapping("/showAllFarmer")
 public List<Farmer> showAllFarmer() {
 List<Farmer> farmer = farmerDao.getAllFarmer();
 return farmer;
}

@GetMapping("/showFarmerById/{farmer_Id}")
 public Farmer showFarmerById(@PathVariable("farmer_Id")  int farmer_Id) {

	 return farmerDao.getfarmerById(farmer_Id);
	 
 }
 
@GetMapping("/showFarmerByName/{farmer_Name}")
public Farmer showFarmerByName(@PathVariable("farmer_Name") String farmer_Name) {
	return farmerDao.getFarmerByName(farmer_Name);	
	
}
@GetMapping("/flogin/{email_Id}/{password}")
public Farmer userAuthentication(@PathVariable("email_Id") String email_Id,
		@PathVariable("password") String password) {
	try {
		Farmer farmerobj=farmerDao.userAuthentication(email_Id);
		System.out.println(farmerobj);
		if(farmerobj!=null) {
		String encodedPassword = farmerobj.getPassword();
		if( pwEncoder.matches(password, encodedPassword)) {
			
			return farmerobj;
		}}}
		catch(Exception e) {
			return null;
		}
	
		return null;
}

@GetMapping("/encodeFarmerPassword/{password}")
public String encodeCustomerPassword(@PathVariable("password") String password) {
	Farmer farmer = new Farmer();
	String encodedPassword = farmer.returnencodedPassword(password);
	return encodedPassword;

}

 @PostMapping("/registerFarmer")
 public Farmer registerFarmer(@RequestBody Farmer farmer) {
	 String emailId=farmer.getEmail_Id();
		String subj="Registration";
		String msg="Successful Registration!\r\n"
				+ "\r\n"
				+ "Dear "+farmer.getFarmer_Name()+"\r\n"
				+ "\r\n"
				+ "Congratulations on successfully registering for FarmConnect! Welcome to our thriving community of passionate farmers and agriculture enthusiasts.\n"+
				"We are excited to have you on board and look forward to seeing your success within our farming community.";
		Farmer encoded = new Farmer(farmer.getFarmer_Id(), farmer.getFarmer_Name(), farmer.getFarmer_Address(), farmer.getEmail_Id(), farmer.getencodedPassword(), farmer.getMobile_Number(), farmer.getProdList());
				
 	boolean flag= EmailService.sendEmail(msg,subj,emailId,"smaaza14@gmail.com");
	 return farmerDao.registerFarmer(encoded);
 }
 
 @PutMapping("/updateFarmer")
 public Farmer updateFarmer(@RequestBody Farmer farmer) {
	 return farmerDao.updateFarmer(farmer);
 }
 
 @DeleteMapping("/deleteFarmer/{farmer_Id}")
	 public void deleteFarmerById(@PathVariable("farmer_Id") int farmer_Id) {
		 farmerDao.deleteFarmerById(farmer_Id);
		 
	 
 }
 
 @GetMapping("/sendsms/{phoneNo}")
 public int sendSMS(@PathVariable("phoneNo") String phoneNo) {
		
 	int min=100000;
 	int max=999999;
 	int otp=(int)(Math.random()*(max-min+1)+min);
 	String msg="Hi! Your FarmConnect OTP for password reset is "+otp+
 	". Please enter this code on the website to reset your password. If you didn't initiate this request, please disregard this message.";
 	System.out.println(otp);
 	
   String s=  smsService.sendSMS(phoneNo, msg);
   if(s!=null){
 	  return otp;
   }
   else{
 	  return 0;
   }
 
}
 
 @GetMapping("/sendOTPbyEmail/{emailId}")
	public int sendOTPByEmail(@PathVariable("emailId") String emailId){
		int min=100000;
 	int max=999999;
 	int otp=(int)(Math.random()*(max-min+1)+min);
 	String msg="Hi! Your FarmConnect OTP for password reset is "+otp+
 	". Please enter this code on the website to reset your password. If you didn't initiate this request, please disregard this message.";
 	System.out.println(otp);
 	String subj="otp service";
 	boolean flag= EmailService.sendEmail(msg,subj,emailId,"smaaza14@gmail.com");
 	if(flag){
 		return otp;
 	}
 	else{
 		return 0;
 	}

}
 
 @PutMapping("/updatePass")
	public int updatPass(@RequestBody Farmer farmer) {
		String emailId = farmer.getEmail_Id();
		String password = farmer.getencodedPassword();
		System.out.println(emailId+" "+password);
		return farmerDao.setPass(emailId, password);

	}

 @PutMapping("/updatePassBysms")
	public int updatePass(@RequestBody Farmer farmer) {
		Long phoneNo =farmer.getMobile_Number();
		String password = farmer.getencodedPassword();
		System.out.println(phoneNo);
		System.out.println(password);
		return farmerDao.setPassword(phoneNo, password);

	}
 
}
