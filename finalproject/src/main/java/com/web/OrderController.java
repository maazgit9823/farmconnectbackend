package com.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.dao.OrderDao;
import com.model.Order;

@RestController
public class OrderController {
	@Autowired
	OrderDao orderDao;
	@GetMapping("/showAllorder")
	public List<Order> showAllOrder() {
		List<Order> order  = orderDao.getAllOrder();
		return order;
	}
	@GetMapping("/showOrderById/{order_Id}")
	public Order ShowOrderById(@PathVariable("order_Id") int order_Id) {
		Order order = orderDao.getOrderById(order_Id);;
		return order;
	}
	
	
	

}
