package com.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.CustomerDao;
import com.dao.EmailService;
import com.dao.SmsService;
import com.model.Customer;
import com.model.Farmer;

 @RestController
 public class CustomerController {
		private static final BCryptPasswordEncoder pwEncoder = new BCryptPasswordEncoder();
		 	
    @Autowired
	CustomerDao customerDao;
    
    @Autowired
    SmsService smsService;
    
    @Autowired
    EmailService EmailService;
    
    @GetMapping("/showAllCustomer")
    public List<Customer> showAllCustomer() {
    	List<Customer> customerList = customerDao.getAllCustomer();
    	return customerList;	
    }
    
	@GetMapping("/showCustomerById/{customer_Id}")
    public Customer showCustomerById(@PathVariable("customer_Id") int customer_Id ) {
    	Customer customer = customerDao.showCustomerById(customer_Id);
    	return customer;	
    }
	
	@GetMapping("/showCustomerByName/{customer_Name}")
	public Customer showCustomerByName(@PathVariable("customer_Name") String customer_Name) {
		Customer customer = customerDao.showCustomerByName(customer_Name);	
		return customer;
	} 
	
	@GetMapping("/clogin/{email_Id}/{password}")
	public Customer userAuthentication(@PathVariable("email_Id") String email_Id,
			@PathVariable("password") String password) {
		try {
			Customer customerobj=customerDao.userAuthentication(email_Id);
			System.out.println(customerobj);
			if(customerobj!=null) {
			String encodedPassword = customerobj.getPassword();
			if( pwEncoder.matches(password, encodedPassword)) {
				
				return customerobj;
			}}
		}
		catch(Exception e) {
			return null;
		}
		return null;
	}

	@GetMapping("/encodeCustomerPassword/{password}")
	public String encodeCustomerPassword(@PathVariable("password") String password) {
		Customer customer=new  Customer();
		String encodedPassword = customer.returnencodedPassword(password);
		return encodedPassword;

	}	
	
	@PostMapping("/registerCustomer")
	public Customer registerCustomer(@RequestBody Customer customer) {
		String emailId=customer.getEmail_Id();
		String subj="Registration";
		String msg="Successful Registration!\r\n"
				+ "\r\n"
				+ "Dear "+customer.getCustomer_Name()+"\r\n"
				+ "\r\n"
				+ "Congratulations on successfully registering for FarmConnect! Welcome to our thriving community of passionate farmers and agriculture enthusiasts.\n"+
				"We are excited to have you on board and look forward to seeing your success within our farming community.";
		Customer encoded = new Customer(customer.getCustomer_Id(), customer.getCustomer_Name(), customer.getCustomer_address(), customer.getEmail_Id(), customer.getencodedPassword(), customer.getMobile_Number(), customer.getOrderList());
		
				
    	boolean flag= EmailService.sendEmail(msg,subj,emailId,"smaaza14@gmail.com");
		 
		return customerDao.registerCustomer(encoded);
	}
    @PutMapping("/updateCustomer")
	public Customer updateCustomer(@RequestBody Customer customer) {
	
		return customerDao.registerCustomer(customer);
	}
	@DeleteMapping("/deleteCustomer/{customer_Id}")
	public void deleteCustomer(@PathVariable("customer_Id") int customer_Id) { 
		customerDao.deleteCustomer(customer_Id);
		
	}
	
	@GetMapping("/send-sms/{phoneNo}")
    public int sendSMS(@PathVariable("phoneNo") String phoneNo) {
		
    	int min=100000;
    	int max=999999;
    	int otp=(int)(Math.random()*(max-min+1)+min);
    	String msg="Hi! Your FarmConnect OTP for password reset is "+otp+
    	". Please enter this code on the website to reset your password. If you didn't initiate this request, please disregard this message.";
    	
    	
      String s=  smsService.sendSMS(phoneNo, msg);
      if(s!=null){
    	  return otp;
      }
      else{
    	  return 0;
      }
    
 }
	@PutMapping("/UpdatePass")
	public int updatPass(@RequestBody Customer customer) {
		String emailId = customer.getEmail_Id();
		String password = customer.getencodedPassword();
		System.out.println(emailId+" "+password);
		return customerDao.setPass(emailId, password);

	}
	
	@PutMapping("/updatePassBySMS")
	public int updatePass(@RequestBody Customer customer) {
		Long phoneNo =customer.getMobile_Number();
		String password = customer.getencodedPassword();
		System.out.println(phoneNo);
		System.out.println(password);
		return customerDao.setPassword(phoneNo, password);

	}


//	@GetMapping("/sendOTPByEmail/{emailId}")
//	public int sendOTPByEmail(@PathVariable("emailId") String emailId){
//		int min=100000;
//    	int max=999999;
//    	int otp=(int)(Math.random()*(max-min+1)+min);
//    	String msg="Hi! Your FarmConnect OTP for password reset is "+otp+
//    	". Please enter this code on the website to reset your password. If you didn't initiate this request, please disregard this message.";
//    	
//    	String subj="otp service";
//    	boolean flag= EmailService.sendEmail(msg,subj,emailId,"smaaza14@gmail.com");
//    	if(flag){
//    		return otp;
//    	}
//    	else{
//    		return 0;
//    	}
//   
//}
	
 }
