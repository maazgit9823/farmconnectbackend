package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Order;
@Service
public class OrderDao {

	@Autowired
OrderRepository orderrepo;

	public List<Order> getAllOrder() {
		return orderrepo.findAll();
	}

	public Order getOrderById(int order_Id) {
	Order order = new Order(0,"no orders find with this id",null,null);
		return orderrepo.findById(order_Id).orElse(order);
	}
//public Order getOrderByCustomerId(int customer_Id) {
//	return orderrepo.findByCustomerId();
//}
}
