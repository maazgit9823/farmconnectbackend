package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Farmer;
@Service
public class FarmerDao {
 
	@Autowired
	FarmerRepository farmerrepo;
	
	public List<Farmer> getAllFarmer() {

		return farmerrepo.findAll();
	}

	public Farmer getfarmerById(int farmer_Id) {  
		
		Farmer farmer = new Farmer(0, "farmer not found with this id ", null, null, null, 0, null);
		return farmerrepo.findById(farmer_Id).orElse(farmer);
	
	}

	public Farmer getFarmerByName(String farmer_Name) {
		Farmer farmer =  farmerrepo.findByName(farmer_Name);
		if(farmer!= null)
		return	farmer;
		else
		return new Farmer(0, "farmer not found", null, null, null, 0, null); 
	
	}

	public Farmer registerFarmer(Farmer farmer) {
		return farmerrepo.save(farmer);
	}

	public Farmer updateFarmer(Farmer farmer) {
     
		return farmerrepo.save(farmer);
		
	}

	public void deleteFarmerById(int farmer_Id) {
		farmerrepo.deleteById(farmer_Id);
		
	}

	public Farmer userAuthentication(String email_Id) {
		return farmerrepo.userAuthentication(email_Id);
	}

	public int setPass(String emailId, String password) {
		return farmerrepo.setPass(emailId,password);
	}

	public int setPassword(Long phoneNo, String password) {
		return farmerrepo.setPassword(phoneNo,password);
	}

}
