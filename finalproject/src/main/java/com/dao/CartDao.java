package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Cart;

@Service
public class CartDao {
	
	@Autowired
	CartRepository cartrepo;

	public List<Cart> showAllCartProducts() {
		return cartrepo.findAll();
	}

	public Cart showCartProductById(int cartId) {
		Cart cart = new Cart(0, 0, "-", 0.0, "-", 0);
		return cartrepo.findById(cartId).orElse(cart);
	}

	public Cart addProductstoCart(Cart cart) {
		return cartrepo.save(cart);
	}

}
