package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Customer;


@Service
public class CustomerDao {
 
	@Autowired
 CustomerRepository customerrepo;

public List<Customer> getAllCustomer() {
	return customerrepo.findAll() ;
}

public Customer showCustomerById(int customer_Id) {
	Customer customer = new Customer(0, "customer Not Found", "-", null, null, 0, null);
	return customerrepo.findById(customer_Id).orElse(customer);
}

public Customer showCustomerByName(String customer_Name) {
	Customer customer = customerrepo.findByName(customer_Name);
	if(customer!= null)
		return customer;
	else
	return new Customer(0, "customer Not Found", "-", null, null, 0, null);
}

public Customer registerCustomer(Customer customer) {

	return customerrepo.save(customer);
}

public Customer updateCutomer(Customer customer) {
	return customerrepo.save(customer);
}
	
	public void deleteCustomer(int customer_Id) {
		customerrepo.deleteById(customer_Id);
	}

	public Customer userAuthentication(String email_Id) {
		return customerrepo.userAuthentication(email_Id);
	}

	public int setPass(String emailId, String password) {
		return customerrepo.setPass(emailId,password);
	}

	public int setPassword(Long phoneNo, String password) {
		return customerrepo.setPassword(phoneNo,password);
	}
}
