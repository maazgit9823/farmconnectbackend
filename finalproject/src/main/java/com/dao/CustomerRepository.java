package com.dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.model.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Integer>{
 
	@Query("from Customer  c where c.customer_Name =:customer_Name")
    Customer findByName(@Param("customer_Name") String customer_Name);

	
	@Query("from Customer c where c.email_Id =:email_Id")
	Customer userAuthentication(@Param("email_Id") String email_Id);


	@Transactional
	@Modifying
	@Query("UPDATE Customer c SET c.password = :password WHERE c.email_Id = :email_Id")
	int setPass(@Param("email_Id") String emailId, @Param("password") String password);


	@Transactional
	@Modifying
	@Query("UPDATE Customer c SET c.password = :password WHERE c.mobile_Number = :mobile_Number")
	public int setPassword(@Param("mobile_Number") Long mobile_Number, @Param("password") String password);

}
