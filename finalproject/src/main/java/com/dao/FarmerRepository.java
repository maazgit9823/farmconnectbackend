package com.dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.model.Farmer;

public interface FarmerRepository extends JpaRepository<Farmer, Integer> {

	@Query("from Farmer f where f.farmer_Name =: farmer_Name")
	Farmer findByName(@Param("farmer_Name") String farmer_Name);

	
	@Query("from Farmer f where f.email_Id =:email_Id")
	Farmer userAuthentication(String email_Id);

	@Transactional
	@Modifying
	@Query("UPDATE Farmer f SET f.password = :password WHERE f.email_Id = :email_Id")
	int setPass(@Param("email_Id") String emailId, @Param("password") String password);


	@Transactional
	@Modifying
	@Query("UPDATE Farmer f SET f.password = :password WHERE f.mobile_Number = :mobile_Number")
	public int setPassword(@Param("mobile_Number") Long phoneNo, @Param("password") String password);
}
