package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Products;

  @Service
  public class ProductDao {
   
	@Autowired
   ProductRepository productrepo;

	public List<Products> getAllProducts() {
		return productrepo.findAll();
	}

	public Products getProductById(int product_Id) {
    Products product =new Products(0,"no product found",0.0,null, null); 
	return productrepo.findById(product_Id).orElse(product);
	}

	public Products getProductByName(String product_Name) {
		return productrepo.findByName(product_Name);
	}
	
	public Products addProduct(Products products) {
		return productrepo.save(products);
	}
	
	public Products updateProduct(Products products) {
		return productrepo.save(products);
	}
	
	public void deleteProductById(int product_Id) {
		productrepo.deleteById(product_Id);
	}

	public List<Products> getProductByfId(int farmer_Id) {
		// TODO Auto-generated method stub
		return productrepo.getProductByfId(farmer_Id);
	}
	

}
