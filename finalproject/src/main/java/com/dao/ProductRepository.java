package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.model.Products;

public interface ProductRepository extends JpaRepository<Products, Integer>{

	@Query("from Products p where p.product_Name =: product_Name")
	Products findByName(@Param("product_Name") String product_Name);

	
	@Query("from Products p where p.farmer.farmer_Id=:farmer_Id")
	List<Products> getProductByfId(int farmer_Id);
}
