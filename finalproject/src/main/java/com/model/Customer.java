package com.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.fasterxml.jackson.annotation.JsonIgnore;
@Entity
public class Customer {
	private static final BCryptPasswordEncoder pwEncoder = new BCryptPasswordEncoder();
	@Id@GeneratedValue
	private int customer_Id;
	private String customer_Name;
	private String customer_address;
	private String email_Id;
	private String password;
	private long mobile_Number;

	@JsonIgnore
	@OneToMany(mappedBy="customer")
	List<Order> orderList = new ArrayList<Order>();
	
	
	public Customer(int customer_Id, String customer_Name, String customer_address, String email_Id, String password,
			long mobile_Number, List<Order> orderList) {
		super();
		this.customer_Id = customer_Id;
		this.customer_Name = customer_Name;
		this.customer_address = customer_address;
		this.email_Id = email_Id;
		this.password = password;
		this.mobile_Number = mobile_Number;
		this.orderList = orderList;
	}


	public Customer() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	public List<Order> getOrderList() {
		return orderList;
	}
	public void setOrderList(List<Order> orderList) {
		this.orderList = orderList;
	}
	public int getCustomer_Id() {
		return customer_Id;
	}
	public void setCustomer_Id(int customer_Id) {
		this.customer_Id = customer_Id;
	}
	public String getCustomer_Name() {
		return customer_Name;
	}
	public void setCustomer_Name(String customer_Name) {
		this.customer_Name = customer_Name;
	}
	public String getCustomer_address() {
		return customer_address;
	}
	public void setCustomer_address(String customer_address) {
		this.customer_address = customer_address;
	}
	public String getEmail_Id() {
		return email_Id;
	}
	public void setEmail_Id(String email_Id) {
		this.email_Id = email_Id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getencodedPassword() {
		return pwEncoder.encode(password);
	}
	public long getMobile_Number() {
		return mobile_Number;
	}
	public void setMobile_Number(long mobile_Number) {
		this.mobile_Number = mobile_Number;
	}

	public String returnencodedPassword(String password) {
		return pwEncoder.encode(password);
	}


	@Override
	public String toString() {
		return "Customer [customer_Id=" + customer_Id + ", customer_Name=" + customer_Name + ", customer_address="
				+ customer_address + ", email_Id=" + email_Id + ", password=" + password + ", mobile_Number="
				+ mobile_Number + ", orderList=" + orderList + "]";
	}

	
	
}
