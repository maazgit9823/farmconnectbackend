package com.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Products {
	@Id @GeneratedValue
private int product_Id;
private String product_Name;
private double product_Price;
private String product_Desc;
private String product_imgsrc ; 

@JsonIgnore
@ManyToOne
@JoinColumn(name="farmer_Id")
Farmer farmer;

@JsonIgnore
@OneToMany(mappedBy="products")
List<Order> prodList = new ArrayList<Order>();



public Products() {
	super();
	// TODO Auto-generated constructor stub
}

public Products(int product_Id, String product_Name, double product_Price, String product_Desc, Farmer farmer) {
	super();
	this.product_Id = product_Id;
	this.product_Name = product_Name;
	this.product_Price = product_Price;
	this.product_Desc = product_Desc;
	this.farmer = farmer;
	
}

public int getProduct_Id() {
	return product_Id;
}

public void setProduct_Id(int product_Id) {
	this.product_Id = product_Id;
}

public String getProduct_Name() {
	return product_Name;
}

public void setProduct_Name(String product_Name) {
	this.product_Name = product_Name;
}

public double getProduct_Price() {
	return product_Price;
}

public void setProduct_Price(double product_Price) {
	this.product_Price = product_Price;
}

public String getProduct_Desc() {
	return product_Desc;
}

public void setProduct_Desc(String product_Desc) {
	this.product_Desc = product_Desc;
}

public Farmer getFarmer() {
	return farmer;
}

public void setFarmer(Farmer farmer) {
	this.farmer = farmer;
}
public String getProduct_imgsrc() {
	return product_imgsrc;
}

public void setProduct_imgsrc(String product_imgsrc) {
	this.product_imgsrc = product_imgsrc;
}


}
