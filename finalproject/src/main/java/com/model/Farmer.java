package com.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Farmer {
	private static final BCryptPasswordEncoder pwEncoder = new BCryptPasswordEncoder();
@Id 
private int farmer_Id;
private String farmer_Name;
private String farmer_Address;
private String email_Id;
private String password;
private long mobile_Number;
@JsonIgnore
@OneToMany(mappedBy="farmer")
List<Products> prodList = new ArrayList<Products>();
public Farmer() {
	super();
	
}
public Farmer(int farmer_Id, String farmer_Name, String farmer_Address, String email_Id, String password,
		long mobile_Number, List<Products> prodList) {
	super();
	this.farmer_Id = farmer_Id;
	this.farmer_Name = farmer_Name;
	this.farmer_Address = farmer_Address;
	this.email_Id = email_Id;
	this.password = password;
	this.mobile_Number = mobile_Number;
	this.prodList = prodList;
}

public List<Products> getProdList() {
	return prodList;
}
public void setProdList(List<Products> prodList) {
	this.prodList = prodList;
}
public int getFarmer_Id() {
	return farmer_Id;
}
public void setFarmer_Id(int farmer_Id) {
	this.farmer_Id = farmer_Id;
}
public String getFarmer_Name() {
	return farmer_Name;
}
public void setFarmer_Name(String farmer_Name) {
	this.farmer_Name = farmer_Name;
}
public String getFarmer_Address() {
	return farmer_Address;
}
public void setFarmer_Address(String farmer_Address) {
	this.farmer_Address = farmer_Address;
}
public String getEmail_Id() {
	return email_Id;
}
public void setEmail_Id(String email_Id) {
	this.email_Id = email_Id;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
public String getencodedPassword() {
	return pwEncoder.encode(password);
}
public long getMobile_Number() {
	return mobile_Number;
}
public void setMobile_Number(long mobile_Number) {
	this.mobile_Number = mobile_Number;
}
public String returnencodedPassword(String password) {
	return pwEncoder.encode(password);
}
@Override
public String toString() {
	return "Farmer [farmer_Id=" + farmer_Id + ", farmer_Name=" + farmer_Name + ", farmer_Address=" + farmer_Address
			+ ", email_Id=" + email_Id + ", password=" + password + ", mobile_Number=" + mobile_Number + ", prodList="
			+ prodList + "]";
}



}
