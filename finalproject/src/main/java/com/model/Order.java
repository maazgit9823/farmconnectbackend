package com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="Orders")
public class Order {
	@Id @GeneratedValue
	private int order_Id;
	private String order_Type;
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="customer_Id")
	Customer customer;
@JsonIgnore	
@ManyToOne
@JoinColumn(name="product_Id")
Products products;
public Order() {
	super();
	// TODO Auto-generated constructor stub
}
public Order(int order_Id, String order_Type, Customer customer, Products products) {
	super();
	this.order_Id = order_Id;
	this.order_Type = order_Type;
	this.customer = customer;
	this.products = products;
}
public int getOrder_Id() {
	return order_Id;
}
public void setOrder_Id(int order_Id) {
	this.order_Id = order_Id;
}
public String getOrder_Type() {
	return order_Type;
}
public void setOrder_Type(String order_Type) {
	this.order_Type = order_Type;
}
public Customer getCustomer() {
	return customer;
}
public void setCustomer(Customer customer) {
	this.customer = customer;
}
public Products getProducts() {
	return products;
}
public void setProducts(Products products) {
	this.products = products;
}


}
