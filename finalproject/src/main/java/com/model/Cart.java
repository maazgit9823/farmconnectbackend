package com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;



@Entity
public class Cart {
	
	@Id@GeneratedValue
	private int cartId;
	private int product_Id;
	private String product_Name;
	private double product_Price;
	private String product_Desc;
	private int customer_Id;
	public Cart(int cartId, int product_Id, String product_Name, double product_Price, String product_Desc,
			int customer_Id) {
		super();
		this.cartId = cartId;
		this.product_Id = product_Id;
		this.product_Name = product_Name;
		this.product_Price = product_Price;
		this.product_Desc = product_Desc;
		this.customer_Id = customer_Id;
	}
	public Cart() {
		super();
		// TODO Auto-generated constructor stub
	}
	public int getCartId() {
		return cartId;
	}
	public void setCartId(int cartId) {
		this.cartId = cartId;
	}
	public int getProduct_Id() {
		return product_Id;
	}
	public void setProduct_Id(int product_Id) {
		this.product_Id = product_Id;
	}
	public String getProduct_Name() {
		return product_Name;
	}
	public void setProduct_Name(String product_Name) {
		this.product_Name = product_Name;
	}
	public double getProduct_Price() {
		return product_Price;
	}
	public void setProduct_Price(double product_Price) {
		this.product_Price = product_Price;
	}
	public String getProduct_Desc() {
		return product_Desc;
	}
	public void setProduct_Desc(String product_Desc) {
		this.product_Desc = product_Desc;
	}
	public int getCustomer_Id() {
		return customer_Id;
	}
	public void setCustomer_Id(int customer_Id) {
		this.customer_Id = customer_Id;
	}
	@Override
	public String toString() {
		return "Cart [cartId=" + cartId + ", product_Id=" + product_Id + ", product_Name=" + product_Name
				+ ", product_Price=" + product_Price + ", product_Desc=" + product_Desc + ", customer_Id=" + customer_Id
				+ "]";
	}
	
	
	
}
